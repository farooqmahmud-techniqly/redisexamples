﻿namespace FFSignup.Models
{
    public sealed class TeamViewModel
    {
        public string TeamId { get; set; }
        public string Record { get; set; }
        public decimal WinPercentage { get; set; }
        public short PointsFor { get; set; }
        public short PointsAgainst { get; set; }
    }
}