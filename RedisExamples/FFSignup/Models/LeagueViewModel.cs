﻿using System.Collections.Generic;

namespace FFSignup.Models
{
    public sealed class LeagueViewModel
    {
        public IEnumerable<TeamViewModel> TeamViewModels { get; set; }
    }
}