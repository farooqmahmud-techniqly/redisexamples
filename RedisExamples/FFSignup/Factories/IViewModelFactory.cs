﻿using System.Collections.Generic;
using FFSignup.Dto;
using FFSignup.Models;

namespace FFSignup.Factories
{
    public interface IViewModelFactory
    {
        LeagueViewModel FromDto(IEnumerable<Team> teams);
    }
}