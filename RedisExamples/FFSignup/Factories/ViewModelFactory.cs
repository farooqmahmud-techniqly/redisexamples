﻿using System.Collections.Generic;
using FFSignup.Dto;
using FFSignup.Models;

namespace FFSignup.Factories
{
    public sealed class ViewModelFactory : IViewModelFactory
    {
        public LeagueViewModel FromDto(IEnumerable<Team> teams)
        {
            var teamViewModels = new List<TeamViewModel>();

            foreach (var team in teams)
            {
                var gamesPlayed = team.Wins + team.Losses + team.Ties;

                var teamViewModel = new TeamViewModel
                {
                    TeamId = team.TeamId,
                    PointsAgainst = team.PointsAgainst,
                    PointsFor = team.PointsFor,
                    Record = $"{team.Wins}-{team.Losses}-{team.Ties}",
                    WinPercentage = gamesPlayed != 0 ? (decimal) team.Wins/gamesPlayed : 0M
                };

                teamViewModels.Add(teamViewModel);
            }

            return new LeagueViewModel
            {
                TeamViewModels = teamViewModels
            };
        }
    }
}