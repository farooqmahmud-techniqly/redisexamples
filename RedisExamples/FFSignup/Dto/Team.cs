﻿namespace FFSignup.Dto
{
    public class Team
    {
        public string TeamId { get; set; }
        public byte Wins { get; set; }
        public byte Losses { get; set; }
        public byte Ties { get; set; }
        public decimal WinPercentage { get; set; }
        public short PointsFor { get; set; }
        public short PointsAgainst { get; set; }
    }
}