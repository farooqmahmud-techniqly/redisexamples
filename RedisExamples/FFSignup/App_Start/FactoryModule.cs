﻿using Autofac;
using FFSignup.Factories;

namespace FFSignup
{
    internal sealed class FactoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ViewModelFactory>().As<IViewModelFactory>().SingleInstance();
        }
    }
}