﻿using Autofac;
using FFSignup.Repositories;

namespace FFSignup
{
    internal sealed class RepositoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<Repository>().As<IRepository>().SingleInstance();
        }
    }
}