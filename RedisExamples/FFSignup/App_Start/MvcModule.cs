﻿using Autofac;
using Autofac.Integration.Mvc;

namespace FFSignup
{
    internal sealed class MvcModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
        }
    }
}