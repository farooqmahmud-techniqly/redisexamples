﻿using Autofac;

namespace FFSignup
{
    internal sealed class ContainerFactory
    {
        internal static IContainer Create()
        {
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyModules(typeof(MvcApplication).Assembly);
            return builder.Build();
        }
    }
}