﻿using System.Threading.Tasks;
using System.Web.Mvc;
using FFSignup.Factories;
using FFSignup.Repositories;

namespace FFSignup.Controllers
{
    public class SignupController : Controller
    {
        private readonly IRepository _repository;
        private readonly IViewModelFactory _viewModelFactory;

        public SignupController(IRepository repository, IViewModelFactory viewModelFactory)
        {
            _repository = repository;
            _viewModelFactory = viewModelFactory;
        }

        public async Task<ActionResult> FantasyFootballLeague()
        {
            var teams = await _repository.GetTeamsAsync();
            var leagueViewModel = _viewModelFactory.FromDto(teams);
            return View(leagueViewModel);
        }

        public async Task<ActionResult> AddTeam(string teamId)
        {
            await _repository.AddTeamAsync(teamId);
            return RedirectToAction("FantasyFootballLeague", "Signup");
        }
    }
}