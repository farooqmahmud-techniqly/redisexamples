﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FFSignup.Dto;

namespace FFSignup.Repositories
{
    public interface IRepository
    {
        Task<IEnumerable<Team>> GetTeamsAsync();
        Task AddTeamAsync(string teamId);
    }
}