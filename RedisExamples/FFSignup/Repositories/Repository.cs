﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FFSignup.Dto;
using StackExchange.Redis.Extensions.Core;

namespace FFSignup.Repositories
{
    public sealed class Repository : IRepository
    {
        private static readonly string _setKey = "Teams";

        private readonly ICacheClient _cacheClient;

        public Repository(ICacheClient cacheClient)
        {
            _cacheClient = cacheClient;
        }

        public async Task<IEnumerable<Team>> GetTeamsAsync()
        {
            return await _cacheClient.SetMembersAsync<Team>(_setKey);
        }

        public async Task AddTeamAsync(string teamId)
        {
            var team = new Team {TeamId = teamId};
            await _cacheClient.SetAddAsync(_setKey, team);
        }
    }
}