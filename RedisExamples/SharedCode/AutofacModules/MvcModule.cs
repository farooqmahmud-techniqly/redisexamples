﻿using Autofac;
using Autofac.Integration.Mvc;
using MlbStandings;

namespace SharedCode.AutofacModules
{
    public sealed class MvcModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
        }
    }
}