﻿using System;
using System.Collections.Generic;
using System.Text;
using Autofac;
using MlbStandings;

namespace SharedCode
{
    public sealed class ContainerFactory
    {
        public static IContainer Create()
        {
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyModules(typeof(MvcApplication).Assembly);
            return builder.Build();
        }
    }
}
