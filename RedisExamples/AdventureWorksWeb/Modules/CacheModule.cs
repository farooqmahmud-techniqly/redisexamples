﻿using Autofac;
using StackExchange.Redis;
using StackExchange.Redis.Extensions.Core;
using StackExchange.Redis.Extensions.Jil;

namespace AdventureWorksWeb.Modules
{
    public sealed class CacheModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            var mux = ConnectionMultiplexer.Connect(
                new ConfigurationOptions
                {
                    EndPoints = {{"127.0.0.1", 6379}},
                    DefaultDatabase = 0
                });

            var serializer = new JilSerializer();
            var cacheClient = new StackExchangeRedisCacheClient(mux, serializer);

            builder.RegisterInstance(cacheClient).As<ICacheClient>().SingleInstance();
        }
    }
}