﻿using AdventureWorksWeb.Data;
using Autofac;

namespace AdventureWorksWeb.Modules
{
    public sealed class DataContextModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterInstance(new AdventureWorksDbContext()).SingleInstance();
            builder.RegisterInstance(new AdventureWorksDwDbContext()).SingleInstance();
        }
    }
}