﻿using AdventureWorksWeb.Repositories;
using Autofac;

namespace AdventureWorksWeb.Modules
{
    public sealed class RepositoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<DepartmentRepository>().As<IDepartmentRepository>().SingleInstance();
            builder.RegisterType<SalesPersonRepository>().As<ISalesPersonRepository>().SingleInstance();
            builder.RegisterType<ProductReviewRepository>().As<IProductReviewRepository>().SingleInstance();
            builder.RegisterType<ProductInventoryRepository>().As<IProductInventoryRepository>().SingleInstance();
            builder.RegisterType<PurchaseOrderHeaderRepository>().As<IPurchaseOrderHeaderRepository>().SingleInstance();
        }
    }
}