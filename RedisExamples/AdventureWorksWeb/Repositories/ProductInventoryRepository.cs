﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using AdventureWorksWeb.Data;
using AdventureWorksWeb.Data.Dto;
using StackExchange.Redis.Extensions.Core;

namespace AdventureWorksWeb.Repositories
{
    public class ProductInventoryRepository : IProductInventoryRepository
    {
        private static readonly string _cacheKey = "productInventory";
        private readonly AdventureWorksDwDbContext _dataContext;
        private readonly ICacheClient _cacheClient;

        public ProductInventoryRepository(
            AdventureWorksDwDbContext dataContext,
            ICacheClient cacheClient)
        {
            _dataContext = dataContext;
            _cacheClient = cacheClient;
        }


        public async Task<IEnumerable<ProductInventory>> GetProductInventoryAsync()
        {
            if (await _cacheClient.ExistsAsync(_cacheKey))
            {
                return await _cacheClient.GetAsync<IEnumerable<ProductInventory>>(_cacheKey);
            }

            var productInventory = await _dataContext.ProductInventories.ToListAsync();

            foreach (var inventory in productInventory)
            {
                await _cacheClient.AddAsync($"{_cacheKey}:{inventory.ProductKey}:{inventory.DateKey}", inventory);
            }
            
            return productInventory;
        }
    }
}