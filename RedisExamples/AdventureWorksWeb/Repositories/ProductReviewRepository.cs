﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using AdventureWorksWeb.Data;
using AdventureWorksWeb.Data.Dto;
using StackExchange.Redis.Extensions.Core;

namespace AdventureWorksWeb.Repositories
{
    public sealed class ProductReviewRepository : IProductReviewRepository
    {
        private static readonly string _productReviewCacheKey = "productReviews";
        private static readonly string _productReviewIdsCacheKey = "productReviewIds";

        private readonly AdventureWorksDbContext _dataContext;
        private readonly ICacheClient _cacheClient;

        public ProductReviewRepository(
            AdventureWorksDbContext dataContext,
            ICacheClient cacheClient)
        {
            _dataContext = dataContext;
            _cacheClient = cacheClient;
        }

        public async Task<IEnumerable<ProductReview>> GetProductReviewsAsync()
        {
            string hashKey;

            if (await _cacheClient.ExistsAsync(_productReviewIdsCacheKey))
            {
                var productReviews = new List<ProductReview>();
                var productReviewIds = await _cacheClient.Database.SetMembersAsync(_productReviewIdsCacheKey);

                foreach (var productReviewId in productReviewIds)
                {
                    hashKey = $"{_productReviewCacheKey}:{productReviewId}";

                    var productId = await _cacheClient.HashGetAsync<int>(hashKey, "productId");
                    var reviewerName = await _cacheClient.HashGetAsync<string>(hashKey, "reviewerName");
                    var reviewDate = await _cacheClient.HashGetAsync<DateTime>(hashKey, "reviewDate");
                    var emailAddress = await _cacheClient.HashGetAsync<string>(hashKey, "emailAddress");
                    var rating = await _cacheClient.HashGetAsync<int>(hashKey, "rating");
                    var comments = await _cacheClient.HashGetAsync<string>(hashKey, "comments");
                    var modifiedDate = await _cacheClient.HashGetAsync<DateTime>(hashKey, "modifiedDate");

                    var productReview = new ProductReview
                                        {
                                            ProductReviewId = (int)productReviewId,
                                            ProductId = productId,
                                            ReviewerName = reviewerName,
                                            ReviewDate = reviewDate,
                                            EmailAddress = emailAddress,
                                            Rating = rating,
                                            Comments = comments,
                                            ModifiedDate = modifiedDate
                                        };

                    productReviews.Add(productReview);
                }

                return productReviews;
            }
            
            var reviews = await _dataContext.ProductReviews.ToListAsync();
             
            foreach (var review in reviews)
            {
                hashKey = $"{_productReviewCacheKey}:{review.ProductReviewId}";

                await _cacheClient.HashSetAsync(hashKey, "productReviewId", review.ProductReviewId);
                await _cacheClient.HashSetAsync(hashKey, "productId", review.ProductId);
                await _cacheClient.HashSetAsync(hashKey, "reviewerName", review.ReviewerName);
                await _cacheClient.HashSetAsync(hashKey, "reviewDate", review.ReviewDate);
                await _cacheClient.HashSetAsync(hashKey, "emailAddress", review.EmailAddress);
                await _cacheClient.HashSetAsync(hashKey, "rating", review.Rating);
                await _cacheClient.HashSetAsync(hashKey, "comments", review.Comments);
                await _cacheClient.HashSetAsync(hashKey, "modifiedDate", review.ModifiedDate);

                await _cacheClient.Database.SetAddAsync(_productReviewIdsCacheKey, review.ProductReviewId);
            }

            return reviews;
        }
    }
}