﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using AdventureWorksWeb.Data;
using AdventureWorksWeb.Data.Dto;
using StackExchange.Redis.Extensions.Core;

namespace AdventureWorksWeb.Repositories
{
    public sealed class DepartmentRepository : IDepartmentRepository
    {
        private static readonly string _cacheKey = "departments";
        private readonly AdventureWorksDbContext _dataContext;
        private readonly ICacheClient _cacheClient;

        public DepartmentRepository(
            AdventureWorksDbContext dataContext,
            ICacheClient cacheClient)
        {
            _dataContext = dataContext;
            _cacheClient = cacheClient;
        }

        public async Task<IEnumerable<Department>> GetDepartmentsAsync()
        {
            if (await _cacheClient.ExistsAsync(_cacheKey))
            {
                return await _cacheClient.GetAsync<IEnumerable<Department>>(_cacheKey);
            }
            
            var departments = await _dataContext.Departments.ToListAsync();
            await _cacheClient.AddAsync(_cacheKey, departments, TimeSpan.FromSeconds(30));
            return departments;
        }
    }
}