﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AdventureWorksWeb.Data;
using AdventureWorksWeb.Data.Dto;
using StackExchange.Redis;
using StackExchange.Redis.Extensions.Core;

namespace AdventureWorksWeb.Repositories
{
    public sealed class SalesPersonRepository : ISalesPersonRepository
    {
        private static readonly string _cacheKey = "topSalesPeople";
        private readonly AdventureWorksDbContext _dataContext;
        private readonly ICacheClient _cacheClient;

        public SalesPersonRepository(
            AdventureWorksDbContext dataContext,
            ICacheClient cacheClient)
        {
            _dataContext = dataContext;
            _cacheClient = cacheClient;
        }

        public async Task<IEnumerable<SalesPerson>> GetTopSalesPeopleAsync()
        {
            const int numberToReturn = 5;
            var salesPeople = new List<SalesPerson>();

            if (await _cacheClient.ExistsAsync(_cacheKey))
            {
                var sortedSet = await _cacheClient.Database.SortedSetRangeByRankWithScoresAsync(
                    _cacheKey,
                    stop: numberToReturn - 1,
                    order: Order.Descending);

                foreach (var sortedSetEntry in sortedSet)
                {
                    var salesPerson = _cacheClient.Serializer.Deserialize<SalesPerson>(sortedSetEntry.Element);
                    salesPerson.SalesYtd = (decimal)sortedSetEntry.Score;

                    salesPeople.Add(salesPerson);
                }

                return salesPeople;
            }

            salesPeople = await _dataContext.SalesPersons.ToListAsync();
            var sortedSetEntries = new List<SortedSetEntry>();

            foreach (var salesPerson in salesPeople)
            {
                var setEntry = new
                                     {
                                        salesPerson.BusinessEntityId,
                                        salesPerson.FirstName,
                                        salesPerson.LastName
                                     };

                sortedSetEntries.Add(new SortedSetEntry(_cacheClient.Serializer.Serialize(setEntry), (double)salesPerson.SalesYtd));
            }

            await _cacheClient.Database.SortedSetAddAsync(
                _cacheKey,
                sortedSetEntries.ToArray());

            return salesPeople.OrderByDescending(sp => sp.SalesYtd).Take(numberToReturn);
        }
    }
}