﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using AdventureWorksWeb.Data.Dto;

namespace AdventureWorksWeb.Repositories
{
    public interface IDepartmentRepository
    {
        Task<IEnumerable<Department>> GetDepartmentsAsync();
    }
}