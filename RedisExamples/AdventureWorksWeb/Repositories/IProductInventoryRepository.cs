﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AdventureWorksWeb.Data.Dto;

namespace AdventureWorksWeb.Repositories
{
    public interface IProductInventoryRepository
    {
        Task<IEnumerable<ProductInventory>> GetProductInventoryAsync();
    }
}