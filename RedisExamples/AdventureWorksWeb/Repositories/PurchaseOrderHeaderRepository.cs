﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using AdventureWorksWeb.Data;
using AdventureWorksWeb.Data.Dto;
using StackExchange.Redis.Extensions.Core;

namespace AdventureWorksWeb.Repositories
{
    public sealed class PurchaseOrderHeaderRepository : IPurchaseOrderHeaderRepository
    {
        private readonly AdventureWorksDbContext _dataContext;
        private readonly ICacheClient _cacheClient;

        public PurchaseOrderHeaderRepository(
            AdventureWorksDbContext dataContext,
            ICacheClient cacheClient)
        {
            _dataContext = dataContext;
            _cacheClient = cacheClient;
        }

        public async Task<IEnumerable<PurchaseOrderHeader>> GetPurchaseOrderHeadersStringAsync()
        {
            var cacheKey = "purchaseOrderHeaderString";

            if (await _cacheClient.ExistsAsync(cacheKey))
            {
                return await _cacheClient.GetAsync<IEnumerable<PurchaseOrderHeader>>(cacheKey);
            }

            var purchaseOrderHeaders = await _dataContext.PurchaseOrderHeaders.ToListAsync();
            await _cacheClient.AddAsync(cacheKey, purchaseOrderHeaders);
            return purchaseOrderHeaders;
        }

        public async Task<IEnumerable<PurchaseOrderHeader>> GetPurchaseOrderHeadersSetAsync()
        {
            var setKey = "purchaseOrderHeaderSet";

            if (await _cacheClient.ExistsAsync(setKey))
            {
                return await _cacheClient.SetMembersAsync<PurchaseOrderHeader>(setKey);
            }

            var purchaseOrderHeaders = await _dataContext.PurchaseOrderHeaders.ToListAsync();

            foreach (var purchaseOrderHeader in purchaseOrderHeaders)
            {
                var memberKey = $"{setKey}:{purchaseOrderHeader.PurchaseOrderId}";
                await _cacheClient.SetAddAsync(memberKey, purchaseOrderHeader);
            }
            
            return purchaseOrderHeaders;
        }

        public async Task<IEnumerable<PurchaseOrderHeader>> GetPurchaseOrderHeadersHashSetAsync()
        {
            var hashSetKey = "purchaseOrderHeaderHashSet";

            if (await _cacheClient.ExistsAsync(hashSetKey))
            {
                var items = await _cacheClient.HashScanAsync<PurchaseOrderHeader>(hashSetKey, "*");
                return new List<PurchaseOrderHeader>();
            }

            var purchaseOrderHeaders = await _dataContext.PurchaseOrderHeaders.ToListAsync();

            foreach (var purchaseOrderHeader in purchaseOrderHeaders)
            {
                var memberKey = $"{hashSetKey}:{purchaseOrderHeader.PurchaseOrderId}";
                await _cacheClient.HashSetAsync(memberKey, "revisionNumber", purchaseOrderHeader.RevisionNumber);
                await _cacheClient.HashSetAsync(memberKey, "status", purchaseOrderHeader.Status);
                await _cacheClient.HashSetAsync(memberKey, "employeeId", purchaseOrderHeader.EmployeeId);
                await _cacheClient.HashSetAsync(memberKey, "vendorId", purchaseOrderHeader.VendorId);
                await _cacheClient.HashSetAsync(memberKey, "shipMethodId", purchaseOrderHeader.ShipMethodId);
                await _cacheClient.HashSetAsync(memberKey, "orderDate", purchaseOrderHeader.OrderDate);
                await _cacheClient.HashSetAsync(memberKey, "shipDate", purchaseOrderHeader.ShipDate);
                await _cacheClient.HashSetAsync(memberKey, "subTotal", purchaseOrderHeader.SubTotal);
                await _cacheClient.HashSetAsync(memberKey, "taxAmt", purchaseOrderHeader.TaxAmt);
                await _cacheClient.HashSetAsync(memberKey, "freight", purchaseOrderHeader.Freight);
                await _cacheClient.HashSetAsync(memberKey, "totalDue", purchaseOrderHeader.TotalDue);
                await _cacheClient.HashSetAsync(memberKey, "modifiedDate", purchaseOrderHeader.ModifiedDate);
            }

            return purchaseOrderHeaders;
        }
    }
}