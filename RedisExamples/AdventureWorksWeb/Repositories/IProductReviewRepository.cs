﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AdventureWorksWeb.Data.Dto;

namespace AdventureWorksWeb.Repositories
{
    public interface IProductReviewRepository
    {
        Task<IEnumerable<ProductReview>> GetProductReviewsAsync();
    }
}