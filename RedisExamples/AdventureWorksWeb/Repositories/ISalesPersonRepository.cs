﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AdventureWorksWeb.Data.Dto;

namespace AdventureWorksWeb.Repositories
{
    public interface ISalesPersonRepository
    {
        Task<IEnumerable<SalesPerson>> GetTopSalesPeopleAsync();
    }
}