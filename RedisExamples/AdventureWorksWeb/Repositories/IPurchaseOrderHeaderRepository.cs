﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AdventureWorksWeb.Data.Dto;

namespace AdventureWorksWeb.Repositories
{
    public interface IPurchaseOrderHeaderRepository
    {
        Task<IEnumerable<PurchaseOrderHeader>> GetPurchaseOrderHeadersStringAsync();
        Task<IEnumerable<PurchaseOrderHeader>> GetPurchaseOrderHeadersSetAsync();
        Task<IEnumerable<PurchaseOrderHeader>> GetPurchaseOrderHeadersHashSetAsync();
    }
}