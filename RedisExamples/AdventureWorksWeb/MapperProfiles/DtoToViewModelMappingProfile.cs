﻿using AdventureWorksWeb.Data.Dto;
using AdventureWorksWeb.Models;
using AutoMapper;

namespace AdventureWorksWeb.MapperProfiles
{
    public sealed class DtoToViewModelMappingProfile : Profile
    {
        public DtoToViewModelMappingProfile() : base("DtoToViewModelMappingProfile")
        {
            CreateMap<Department, DepartmentViewModel>();
            CreateMap<SalesPerson, SalesPersonViewModel>();
            CreateMap<ProductReview, ProductReviewViewModel>();
            CreateMap<PurchaseOrderHeader, PurchaseOrderHeaderViewModel>();
        }


    }
}