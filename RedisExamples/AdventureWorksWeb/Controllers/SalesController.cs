﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using AdventureWorksWeb.Models;
using AdventureWorksWeb.Repositories;
using AutoMapper;

namespace AdventureWorksWeb.Controllers
{
    public class SalesController : Controller
    {
        private readonly ISalesPersonRepository _salesPersonRepository;
        private readonly IMapper _mapper;

        public SalesController(
            ISalesPersonRepository salesPersonRepository,
            IMapper mapper)
        {
            _salesPersonRepository = salesPersonRepository;
            _mapper = mapper;
        }

        public async Task<ActionResult> TopSalesPeople()
        {
            var salesPeople = await _salesPersonRepository.GetTopSalesPeopleAsync();
            var salesPeopleViewModel = _mapper.Map<IEnumerable<SalesPersonViewModel>>(salesPeople);
            return View(salesPeopleViewModel);
        }
    }
}