﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using AdventureWorksWeb.Models;
using AdventureWorksWeb.Repositories;
using AutoMapper;

namespace AdventureWorksWeb.Controllers
{
    public class DepartmentsController : Controller
    {
        private readonly IDepartmentRepository _departmentRepository;
        private readonly IMapper _mapper;

        public DepartmentsController(
            IDepartmentRepository departmentRepository,
            IMapper mapper)
        {
            _departmentRepository = departmentRepository;
            _mapper = mapper;
        }

        public async Task<ActionResult> Index()
        {
            var departments = await _departmentRepository.GetDepartmentsAsync();
            var departmentViewModels = _mapper.Map<IEnumerable<DepartmentViewModel>>(departments);
            return View(departmentViewModels);
        }
    }
}