﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using AdventureWorksWeb.Models;
using AdventureWorksWeb.Repositories;
using AutoMapper;

namespace AdventureWorksWeb.Controllers
{
    public class PurchaseOrderHeadersController : Controller
    {
        private readonly IPurchaseOrderHeaderRepository _purchaseOrderHeaderRepository;
        private readonly IMapper _mapper;

        public PurchaseOrderHeadersController(
            IPurchaseOrderHeaderRepository purchaseOrderHeaderRepository,
            IMapper mapper)
        {
            _purchaseOrderHeaderRepository = purchaseOrderHeaderRepository;
            _mapper = mapper;
        }

        public async Task<ActionResult> PurchaseOrderHeadersString()
        {
            var purchaseOrderHeaders = await _purchaseOrderHeaderRepository.GetPurchaseOrderHeadersStringAsync();
            var purchaseOrderHeaderViewModels = _mapper.Map<IEnumerable<PurchaseOrderHeaderViewModel>>(purchaseOrderHeaders);
            return View(purchaseOrderHeaderViewModels);
        }

        public async Task<ActionResult> PurchaseOrderHeadersSet()
        {
            var purchaseOrderHeaders = await _purchaseOrderHeaderRepository.GetPurchaseOrderHeadersSetAsync();
            var purchaseOrderHeaderViewModels = _mapper.Map<IEnumerable<PurchaseOrderHeaderViewModel>>(purchaseOrderHeaders);
            return View(purchaseOrderHeaderViewModels);
        }


        public async Task<ActionResult> PurchaseOrderHeadersHashSet()
        {
            var purchaseOrderHeaders = await _purchaseOrderHeaderRepository.GetPurchaseOrderHeadersHashSetAsync();
            var purchaseOrderHeaderViewModels = _mapper.Map<IEnumerable<PurchaseOrderHeaderViewModel>>(purchaseOrderHeaders);
            return View(purchaseOrderHeaderViewModels);
        }
    }
}