﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using AdventureWorksWeb.Models;
using AdventureWorksWeb.Repositories;
using AutoMapper;

namespace AdventureWorksWeb.Controllers
{
    public class ProductReviewsController : Controller
    {
        private readonly IProductReviewRepository _productReviewRepository;
        private readonly IMapper _mapper;

        public ProductReviewsController(
            IProductReviewRepository productReviewRepository,
            IMapper mapper)
        {
            _productReviewRepository = productReviewRepository;
            _mapper = mapper;
        }

        public async Task<ActionResult> Index()
        {
            var productReviews = await _productReviewRepository.GetProductReviewsAsync();
            var productReviewsViewModel = _mapper.Map<IEnumerable<ProductReviewViewModel>>(productReviews);
            return View(productReviewsViewModel);
        }
    }
}