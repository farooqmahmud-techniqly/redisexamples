﻿using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using AdventureWorksWeb.Models;
using AdventureWorksWeb.Repositories;
using AutoMapper;

namespace AdventureWorksWeb.Controllers
{
    public class InventoryController : Controller
    {
        private readonly IProductInventoryRepository _productInventoryRepository;
        private readonly IMapper _mapper;

        public InventoryController(
            IProductInventoryRepository productInventoryRepository, 
            IMapper mapper)
        {
            _productInventoryRepository = productInventoryRepository;
            _mapper = mapper;
        }

        public async Task<ActionResult> Index()
        {
            var inventory = await _productInventoryRepository.GetProductInventoryAsync();
            var inventoryViewModel = new ProductInventoryViewModel {Count = inventory.Count()};
            return View(inventoryViewModel);
        }
    }
}