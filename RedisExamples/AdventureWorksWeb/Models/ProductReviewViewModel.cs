﻿namespace AdventureWorksWeb.Models
{
    public sealed class ProductReviewViewModel
    {
        public string ReviewerName { get; set; }
        public int Rating { get; set; }
        public string Comments { get; set; }
        public int ProductId { get; set; }
    }
}