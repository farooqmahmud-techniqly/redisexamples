﻿namespace AdventureWorksWeb.Models
{
    public sealed class ProductInventoryViewModel
    {
        public int Count { get; set; }
    }
}