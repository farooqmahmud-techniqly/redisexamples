﻿namespace AdventureWorksWeb.Models
{
    public class DepartmentViewModel
    {
        public string Name { get; set; }
        public string GroupName { get; set; }
    }
}