﻿namespace AdventureWorksWeb.Models
{
    public sealed class SalesPersonViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal SalesYtd { get; set; }
    }
}