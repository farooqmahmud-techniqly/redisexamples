﻿using Autofac;

namespace AdventureWorksWeb
{
    public sealed class ContainerFactory
    {
        internal static IContainer Create()
        {
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyModules(typeof(MvcApplication).Assembly);
            var container = builder.Build();
            return container;
        }
    }
}