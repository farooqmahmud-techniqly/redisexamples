﻿using System.Data.Entity;
using AdventureWorksWeb.Data.Dto;
using AdventureWorksWeb.Data.Mapping;

namespace AdventureWorksWeb.Data
{
    public sealed class AdventureWorksDbContext : DbContext
    {
        static AdventureWorksDbContext()
        {
            Database.SetInitializer<AdventureWorksDbContext>(null);
        }

        public AdventureWorksDbContext()
            : base("awdb")
        {
        }

        public DbSet<Department> Departments { get; set; }
        public DbSet<SalesPerson> SalesPersons { get; set; }
        public DbSet<ProductReview> ProductReviews { get; set; }
        public DbSet<PurchaseOrderHeader> PurchaseOrderHeaders { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new DepartmentMap());
            modelBuilder.Configurations.Add(new SalesPersonsMap());
            modelBuilder.Configurations.Add(new ProductReviewMap());
            modelBuilder.Configurations.Add(new PurchaseOrderHeaderMap());
        }
    }
}