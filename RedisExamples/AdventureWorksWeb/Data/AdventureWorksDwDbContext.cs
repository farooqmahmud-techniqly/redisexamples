﻿using System.Data.Entity;
using AdventureWorksWeb.Data.Dto;
using AdventureWorksWeb.Data.Mapping;

namespace AdventureWorksWeb.Data
{
    public sealed class AdventureWorksDwDbContext : DbContext
    {
        static AdventureWorksDwDbContext()
        {
            Database.SetInitializer<AdventureWorksDwDbContext>(null);
        }

        public AdventureWorksDwDbContext():base("awdwdb")
        {
            
        }

        public DbSet<ProductInventory> ProductInventories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ProductInventoryMap());
        }
    }
}