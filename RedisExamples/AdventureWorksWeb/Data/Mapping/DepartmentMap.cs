﻿using System.Data.Entity.ModelConfiguration;
using AdventureWorksWeb.Data.Dto;

namespace AdventureWorksWeb.Data.Mapping
{
    public sealed class DepartmentMap : EntityTypeConfiguration<Department>
    {
        public DepartmentMap()
        {
            HasKey(t => t.DepartmentId);
            
            Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.GroupName)
                .IsRequired()
                .HasMaxLength(50);

            ToTable("Department", "HumanResources");
            Property(t => t.DepartmentId).HasColumnName("DepartmentID");
            Property(t => t.Name).HasColumnName("Name");
            Property(t => t.GroupName).HasColumnName("GroupName");
            Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
        }
    }
}