﻿using System.Data.Entity.ModelConfiguration;
using AdventureWorksWeb.Data.Dto;

namespace AdventureWorksWeb.Data.Mapping
{
    public sealed class PurchaseOrderHeaderMap : EntityTypeConfiguration<PurchaseOrderHeader>
    {
        public PurchaseOrderHeaderMap()
        {
            HasKey(t => t.PurchaseOrderId);
            
            ToTable("PurchaseOrderHeader", "Purchasing");
            Property(t => t.PurchaseOrderId).HasColumnName("PurchaseOrderID");
            Property(t => t.RevisionNumber).HasColumnName("RevisionNumber");
            Property(t => t.Status).HasColumnName("Status");
            Property(t => t.EmployeeId).HasColumnName("EmployeeID");
            Property(t => t.VendorId).HasColumnName("VendorID");
            Property(t => t.ShipMethodId).HasColumnName("ShipMethodID");
            Property(t => t.OrderDate).HasColumnName("OrderDate");
            Property(t => t.ShipDate).HasColumnName("ShipDate");
            Property(t => t.SubTotal).HasColumnName("SubTotal");
            Property(t => t.TaxAmt).HasColumnName("TaxAmt");
            Property(t => t.Freight).HasColumnName("Freight");
            Property(t => t.TotalDue).HasColumnName("TotalDue");
            Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
        }
    }
}