﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using AdventureWorksWeb.Data.Dto;

namespace AdventureWorksWeb.Data.Mapping
{
    public sealed class SalesPersonsMap : EntityTypeConfiguration<SalesPerson>
    {
        public SalesPersonsMap()
        {
            HasKey(t => new { t.BusinessEntityId, t.FirstName, t.LastName, t.JobTitle, t.EmailPromotion, t.AddressLine1, t.City, t.StateProvinceName, t.PostalCode, t.CountryRegionName, t.SalesYtd, t.SalesLastYear });

            // Properties
            Property(t => t.BusinessEntityId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.Title)
                .HasMaxLength(8);

            Property(t => t.FirstName)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.MiddleName)
                .HasMaxLength(50);

            Property(t => t.LastName)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.Suffix)
                .HasMaxLength(10);

            Property(t => t.JobTitle)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.PhoneNumber)
                .HasMaxLength(25);

            Property(t => t.PhoneNumberType)
                .HasMaxLength(50);

            Property(t => t.EmailAddress)
                .HasMaxLength(50);

            Property(t => t.EmailPromotion)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.AddressLine1)
                .IsRequired()
                .HasMaxLength(60);

            Property(t => t.AddressLine2)
                .HasMaxLength(60);

            Property(t => t.City)
                .IsRequired()
                .HasMaxLength(30);

            Property(t => t.StateProvinceName)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.PostalCode)
                .IsRequired()
                .HasMaxLength(15);

            Property(t => t.CountryRegionName)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.TerritoryName)
                .HasMaxLength(50);

            Property(t => t.TerritoryGroup)
                .HasMaxLength(50);

            Property(t => t.SalesYtd)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.SalesLastYear)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            ToTable("vSalesPerson", "Sales");
            Property(t => t.BusinessEntityId).HasColumnName("BusinessEntityID");
            Property(t => t.Title).HasColumnName("Title");
            Property(t => t.FirstName).HasColumnName("FirstName");
            Property(t => t.MiddleName).HasColumnName("MiddleName");
            Property(t => t.LastName).HasColumnName("LastName");
            Property(t => t.Suffix).HasColumnName("Suffix");
            Property(t => t.JobTitle).HasColumnName("JobTitle");
            Property(t => t.PhoneNumber).HasColumnName("PhoneNumber");
            Property(t => t.PhoneNumberType).HasColumnName("PhoneNumberType");
            Property(t => t.EmailAddress).HasColumnName("EmailAddress");
            Property(t => t.EmailPromotion).HasColumnName("EmailPromotion");
            Property(t => t.AddressLine1).HasColumnName("AddressLine1");
            Property(t => t.AddressLine2).HasColumnName("AddressLine2");
            Property(t => t.City).HasColumnName("City");
            Property(t => t.StateProvinceName).HasColumnName("StateProvinceName");
            Property(t => t.PostalCode).HasColumnName("PostalCode");
            Property(t => t.CountryRegionName).HasColumnName("CountryRegionName");
            Property(t => t.TerritoryName).HasColumnName("TerritoryName");
            Property(t => t.TerritoryGroup).HasColumnName("TerritoryGroup");
            Property(t => t.SalesQuota).HasColumnName("SalesQuota");
            Property(t => t.SalesYtd).HasColumnName("SalesYTD");
            Property(t => t.SalesLastYear).HasColumnName("SalesLastYear");
        }
    }
}