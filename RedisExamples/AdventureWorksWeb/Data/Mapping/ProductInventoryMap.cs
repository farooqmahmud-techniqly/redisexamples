﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using AdventureWorksWeb.Data.Dto;

namespace AdventureWorksWeb.Data.Mapping
{
    public class ProductInventoryMap : EntityTypeConfiguration<ProductInventory>
    {
        public ProductInventoryMap()
        {
            HasKey(t => new { t.ProductKey, t.DateKey });

            Property(t => t.ProductKey)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.DateKey)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            ToTable("FactProductInventory");
            Property(t => t.ProductKey).HasColumnName("ProductKey");
            Property(t => t.DateKey).HasColumnName("DateKey");
            Property(t => t.MovementDate).HasColumnName("MovementDate");
            Property(t => t.UnitCost).HasColumnName("UnitCost");
            Property(t => t.UnitsIn).HasColumnName("UnitsIn");
            Property(t => t.UnitsOut).HasColumnName("UnitsOut");
            Property(t => t.UnitsBalance).HasColumnName("UnitsBalance");
        }
    }
}