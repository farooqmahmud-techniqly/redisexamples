﻿using System.Data.Entity.ModelConfiguration;
using AdventureWorksWeb.Data.Dto;

namespace AdventureWorksWeb.Data.Mapping
{
    public sealed class ProductReviewMap : EntityTypeConfiguration<ProductReview>
    {
        public ProductReviewMap()
        {
            HasKey(t => t.ProductReviewId);

            Property(t => t.ReviewerName)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.EmailAddress)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.Comments)
                .HasMaxLength(3850);
            
            ToTable("ProductReview", "Production");
            Property(t => t.ProductReviewId).HasColumnName("ProductReviewID");
            Property(t => t.ProductId).HasColumnName("ProductID");
            Property(t => t.ReviewerName).HasColumnName("ReviewerName");
            Property(t => t.ReviewDate).HasColumnName("ReviewDate");
            Property(t => t.EmailAddress).HasColumnName("EmailAddress");
            Property(t => t.Rating).HasColumnName("Rating");
            Property(t => t.Comments).HasColumnName("Comments");
            Property(t => t.ModifiedDate).HasColumnName("ModifiedDate");
            
        }
    }
}