﻿using Newtonsoft.Json;

namespace MlbStandings.Dto
{
    public sealed class TeamStanding
    {
        public byte Won { get; set; }
        public byte Lost { get; set; }

        [JsonProperty("first_name")]
        public string Name { get; set; }

        [JsonProperty("last_name")]
        public string Nickname { get; set; }

        public byte Rank { get; set; }

        [JsonProperty("games_back")]
        public decimal GamesBack { get; set; }

        public string Division { get; set; }

        [JsonProperty("Conference")]
        public string League { get; set; }
    }
}