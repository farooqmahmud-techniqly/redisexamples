﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MlbStandings.Dto;

namespace MlbStandings.Repositories
{
    public interface IStandingsRepository
    {
        Task<IEnumerable<TeamStanding>> GetStandingsAsync();
    }
}