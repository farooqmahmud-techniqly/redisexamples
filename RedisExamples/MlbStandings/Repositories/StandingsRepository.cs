﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using MlbStandings.Dto;
using MlbStandings.Properties;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using StackExchange.Redis.Extensions.Core;

namespace MlbStandings.Repositories
{
    public sealed class StandingsRepository : IStandingsRepository
    {
        private static readonly string _standingsKey = "standings";
        private readonly ICacheClient _cacheClient;

        public StandingsRepository(ICacheClient cacheClient)
        {
            _cacheClient = cacheClient;
        }

        public async Task<IEnumerable<TeamStanding>> GetStandingsAsync()
        {
            var standingsExist = await _cacheClient.ExistsAsync(_standingsKey);

            if (standingsExist)
            {
                return await _cacheClient.GetAsync<IEnumerable<TeamStanding>>(_standingsKey);
            }
            
            var jo = JObject.Parse(Resources.StandingsJson);
            var standingsJson = jo["standing"];
            var teamStandings = standingsJson.ToObject<IEnumerable<TeamStanding>>();

            await _cacheClient.AddAsync(_standingsKey, teamStandings, TimeSpan.FromSeconds(10));

            return teamStandings;
        }
    }
}