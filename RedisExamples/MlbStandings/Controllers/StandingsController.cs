﻿using System.Threading.Tasks;
using System.Web.Mvc;
using MlbStandings.Factories;
using MlbStandings.Repositories;

namespace MlbStandings.Controllers
{
    public class StandingsController : Controller
    {
        private readonly IStandingsRepository _standingsRepository;
        private readonly IViewModelFactory _viewModelFactory;

        public StandingsController(IStandingsRepository standingsRepository, IViewModelFactory viewModelFactory)
        {
            _standingsRepository = standingsRepository;
            _viewModelFactory = viewModelFactory;
        }

        public async Task<ActionResult> Index()
        {
            var standings = await _standingsRepository.GetStandingsAsync();
            var standingsViewModel = _viewModelFactory.FromDto(standings);
            return View(standingsViewModel);
        }
    }
}