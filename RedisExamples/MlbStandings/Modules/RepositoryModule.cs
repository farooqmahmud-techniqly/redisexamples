﻿using Autofac;
using MlbStandings.Repositories;

namespace MlbStandings.Modules
{
    public sealed class RepositoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<StandingsRepository>().As<IStandingsRepository>().SingleInstance();
        }
    }
}