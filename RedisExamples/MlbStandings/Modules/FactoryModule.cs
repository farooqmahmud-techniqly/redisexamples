﻿using Autofac;
using MlbStandings.Factories;

namespace MlbStandings.Modules
{
    public sealed class FactoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ViewModelFactory>().As<IViewModelFactory>().SingleInstance();
        }
    }
}