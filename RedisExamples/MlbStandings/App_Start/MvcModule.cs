﻿using Autofac;
using Autofac.Integration.Mvc;

namespace MlbStandings
{
    public sealed class MvcModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterControllers(typeof(MvcApplication).Assembly);
        }
    }
}