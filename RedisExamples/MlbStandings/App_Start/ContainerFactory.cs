﻿using Autofac;

namespace MlbStandings
{
    public sealed class ContainerFactory
    {
        public static IContainer Create()
        {
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyModules(typeof(MvcApplication).Assembly);
            return builder.Build();
        }
    }
}
