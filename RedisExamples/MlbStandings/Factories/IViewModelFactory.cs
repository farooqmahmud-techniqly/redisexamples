﻿using System.Collections.Generic;
using MlbStandings.Dto;
using MlbStandings.Models;

namespace MlbStandings.Factories
{
    public interface IViewModelFactory
    {
        StandingsViewModel FromDto(IEnumerable<TeamStanding> teamStandings);
    }
}