﻿using System.Collections.Generic;
using System.Linq;
using MlbStandings.Dto;
using MlbStandings.Models;

namespace MlbStandings.Factories
{
    public sealed class ViewModelFactory : IViewModelFactory
    {
        public StandingsViewModel FromDto(IEnumerable<TeamStanding> teamStandings)
        {
            var nationalLeagueStandingsViewModel = GetNationalLeagueStandings(teamStandings);
            var americanLeagueStandingsViewModel = GetAmericanLeagueStandings(teamStandings);

            return new StandingsViewModel
            {
                AmericanLeagueStandingsViewModel = americanLeagueStandingsViewModel,
                NationalLeagueStandingsViewModel = nationalLeagueStandingsViewModel
            };
        }

        private static LeagueStandingsViewModel GetNationalLeagueStandings(IEnumerable<TeamStanding> teamStandings)
        {
            return GetLeagueStandings(teamStandings, "NL");
        }

        private static LeagueStandingsViewModel GetAmericanLeagueStandings(IEnumerable<TeamStanding> teamStandings)
        {
            return GetLeagueStandings(teamStandings, "AL");
        }

        private static LeagueStandingsViewModel GetLeagueStandings(IEnumerable<TeamStanding> teamStandings, string league)
        {
            var leagueStandingsViewModel = new LeagueStandingsViewModel();
            var eastDivisionStandingsViewModel = new List<TeamStandingViewModel>();

            foreach (var teamStanding in GetTeamStandings(teamStandings, league, "E"))
            {
                var teamStandingViewModel = ToViewModel(teamStanding);
                eastDivisionStandingsViewModel.Add(teamStandingViewModel);
            }

            leagueStandingsViewModel.EastDivisionStandings = eastDivisionStandingsViewModel;

            var centralDivisionStandingsViewModel = new List<TeamStandingViewModel>();

            foreach (var teamStanding in GetTeamStandings(teamStandings, league, "C"))
            {
                var teamStandingViewModel = ToViewModel(teamStanding);
                centralDivisionStandingsViewModel.Add(teamStandingViewModel);
            }

            leagueStandingsViewModel.CentralDivisionStandings = centralDivisionStandingsViewModel;

            var westDivisionStandingsViewModel = new List<TeamStandingViewModel>();

            foreach (var teamStanding in GetTeamStandings(teamStandings, league, "W"))
            {
                var teamStandingViewModel = ToViewModel(teamStanding);
                westDivisionStandingsViewModel.Add(teamStandingViewModel);
            }

            leagueStandingsViewModel.WestDivisionStandings = westDivisionStandingsViewModel;

            return leagueStandingsViewModel;
        }

        private static IEnumerable<TeamStanding> GetTeamStandings(IEnumerable<TeamStanding> teamStandings,  string league, string division)
        {
            return teamStandings.Where(s => s.League == league && s.Division == division);
        }

        private static TeamStandingViewModel ToViewModel(TeamStanding teamStanding)
        {
            return new TeamStandingViewModel
            {
                Team = $"{teamStanding.Name} {teamStanding.Nickname}",
                Rank = teamStanding.Rank,
                Won = teamStanding.Won,
                Lost = teamStanding.Lost,
                GamesBack = teamStanding.GamesBack
            };
        }
    }
}