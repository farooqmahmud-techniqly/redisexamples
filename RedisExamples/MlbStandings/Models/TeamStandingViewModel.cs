﻿namespace MlbStandings.Models
{
    public sealed class TeamStandingViewModel
    {
        public byte Rank { get; set; }
        public string Team { get; set; }
        public byte Won { get; set; }
        public byte Lost { get; set; }
        public decimal GamesBack { get; set; }
    }
}