﻿namespace MlbStandings.Models
{
    public sealed class StandingsViewModel
    {
        public LeagueStandingsViewModel NationalLeagueStandingsViewModel { get; set; }
        public LeagueStandingsViewModel AmericanLeagueStandingsViewModel { get; set; }

    }
}