﻿using System.Collections.Generic;

namespace MlbStandings.Models
{
    public sealed class LeagueStandingsViewModel
    {
        public IEnumerable<TeamStandingViewModel> EastDivisionStandings { get; set; }
        public IEnumerable<TeamStandingViewModel> CentralDivisionStandings { get; set; }
        public IEnumerable<TeamStandingViewModel> WestDivisionStandings { get; set; }
    }
}